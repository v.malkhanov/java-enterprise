import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;

public class App {

    private final static Logger log = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        if (args.length < 1) {
            return;
        }
        log.info("Hello world!");
        var fileName = args[0];
        try (var in = new BZip2CompressorInputStream(new FileInputStream(fileName))) {
            var counter = new XmlDataCounter(in);
            counter.compute();
            counter.printResult(System.out);
        } catch (XMLStreamException | IOException ex) {
            log.error("Error", ex);
        }
    }
}
