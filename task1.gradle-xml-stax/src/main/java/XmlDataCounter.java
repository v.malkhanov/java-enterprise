import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Map;
import java.util.TreeMap;

public class XmlDataCounter {
    private final static Logger log = LogManager.getLogger(XmlDataCounter.class);
    private final static XMLInputFactory factory = XMLInputFactory.newInstance();

    private final TreeMap<String, Long> userCount = new TreeMap<>();
    private final TreeMap<String, Long> keyCount = new TreeMap<>();

    private final XMLStreamReader reader;

    public XmlDataCounter(InputStream in) throws XMLStreamException {
        this.reader = factory.createXMLStreamReader(in);
    }

    public void compute() throws XMLStreamException {
        while (hasNextTag("node")) {
            userCount.compute(getAttribute("user"), (key, value) -> (value == null) ? 1L : ++value);
            while (hasNextTag("tag", "node")) {
                keyCount.compute(getAttribute("k"), (key, value) -> (value == null) ? 1L : ++value);
            }
        }

        log.info("User counts: {}", userCount.values().stream().mapToLong(Long::longValue).sum());
        log.info("Keys counts: {}", keyCount.values().stream().mapToLong(Long::longValue).sum());
    }

    public void printResult(PrintStream out) {
        out.println("User - count:");
        userCount.entrySet()
            .stream()
            .sorted(Map.Entry.comparingByValue((a, b) -> -Long.compare(a, b)))
            .forEach(entry -> out.printf("%-25s: %d%n", entry.getKey(), entry.getValue()));

        out.println("\n\nKey - count:");
        keyCount.entrySet()
            .stream()
            .sorted(Map.Entry.comparingByValue((a, b) -> -Long.compare(a, b)))
            .forEach(entry -> out.printf("%-30s: %d%n", entry.getKey(), entry.getValue()));
    }

    private boolean hasNextTag(String tag) throws XMLStreamException {
        while (reader.hasNext()) {
            var element = reader.next();
            if (element == XMLEvent.START_ELEMENT && tag.equals(reader.getLocalName())) {
                return true;
            }
        }
        return false;
    }

    private boolean hasNextTag(String tag, String parentTag) throws XMLStreamException {
        while (reader.hasNext()) {
            var element = reader.next();
            if (parentTag != null && element == XMLEvent.END_ELEMENT && parentTag.equals(reader.getLocalName())) {
                return false;
            }
            if (element == XMLEvent.START_ELEMENT && tag.equals(reader.getLocalName())) {
                return true;
            }
        }
        return false;
    }

    private String getAttribute(String name) {
        return reader.getAttributeValue(null, name);
    }
}
