package ru.nsu.fit.malkhanov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.fit.malkhanov.model.Node;

import java.util.List;

public interface NodeRepository extends JpaRepository<Node, Long> {
    @Query(nativeQuery = true,
        value = "SELECT *, earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.latitude, node.longitude)) as distance " +
            "FROM node WHERE earth_distance(ll_to_earth(?1, ?2), ll_to_earth(node.latitude, node.longitude)) < ?3 ORDER BY distance")
    List<Node> findByDistance(double latitude, double longitude, double radius);
}
