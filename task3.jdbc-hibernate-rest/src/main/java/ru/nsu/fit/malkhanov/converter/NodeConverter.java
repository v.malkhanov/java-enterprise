package ru.nsu.fit.malkhanov.converter;

import org.springframework.stereotype.Component;
import ru.nsu.fit.malkhanov.dto.NodeDto;
import ru.nsu.fit.malkhanov.dto.TagDto;
import ru.nsu.fit.malkhanov.model.Node;
import ru.nsu.fit.malkhanov.model.Tag;
import ru.nsu.fit.malkhanov.model.jaxb.NodeOsm;
import ru.nsu.fit.malkhanov.model.jaxb.TagOsm;

import java.time.ZoneOffset;
import java.util.stream.Collectors;

@Component
public class NodeConverter {

    public Node convert(NodeDto node) {
        var entity = new Node();
        entity.setId(node.id);
        entity.setLatitude(node.latitude);
        entity.setLongitude(node.longitude);
        entity.setUsername(node.username);
        entity.setUid(node.uid);
        entity.setChangeSet(node.changeSet);
        entity.setVersion(node.version);
        entity.setVisible(node.visible);
        entity.setTimestamp(node.timestamp);
        entity.setTags(node.tags.stream().map(this::convert).collect(Collectors.toList()));
        return entity;
    }

    public NodeDto convert(Node node) {
        var dto = new NodeDto();
        dto.id = node.getId();
        dto.latitude = node.getLatitude();
        dto.longitude = node.getLongitude();
        dto.username = node.getUsername();
        dto.uid = node.getUid();
        dto.version = node.getVersion();
        dto.changeSet = node.getChangeSet();
        dto.visible = node.getVisible();
        dto.timestamp = node.getTimestamp();
        dto.tags = node.getTags().stream().map(this::convert).collect(Collectors.toList());
        return dto;
    }

    public TagDto convert(Tag tag) {
        var dto = new TagDto();
        dto.key = tag.getKey();
        dto.value = tag.getValue();
        return dto;
    }

    public Node convert(NodeOsm node) {
        var entity = new Node();
        entity.setId(node.getId().longValue());
        entity.setLatitude(node.getLat());
        entity.setLongitude(node.getLon());
        entity.setUsername(node.getUser());
        entity.setUid(node.getUid().longValue());
        entity.setChangeSet(node.getChangeset().longValue());
        entity.setVersion(node.getVersion().longValue());
        entity.setVisible(node.isVisible());
        entity.setTimestamp(node.getTimestamp()
            .toGregorianCalendar()
            .toZonedDateTime()
            .withZoneSameLocal(ZoneOffset.UTC)
            .toInstant());
        entity.setTags(node.getTag().stream().map(tag -> convert(tag, entity)).collect(Collectors.toList()));
        return entity;
    }

    private Tag convert(TagOsm tag, Node node) {
        var entity = new Tag();
        entity.setKey(tag.getK());
        entity.setValue(tag.getV());
        entity.setNode(node);
        return entity;
    }

    private Tag convert(TagDto tag) {
        var entity = new Tag();
        entity.setKey(tag.key);
        entity.setValue(tag.value);
        return entity;
    }

}
