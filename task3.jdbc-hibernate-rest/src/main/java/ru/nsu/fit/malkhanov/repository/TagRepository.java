package ru.nsu.fit.malkhanov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.fit.malkhanov.model.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {
}
