package ru.nsu.fit.malkhanov.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.fit.malkhanov.converter.NodeConverter;
import ru.nsu.fit.malkhanov.dto.NodeDto;
import ru.nsu.fit.malkhanov.model.jaxb.NodeOsm;
import ru.nsu.fit.malkhanov.repository.NodeRepository;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class NodeService {
    private final NodeRepository nodeRepository;
    private final NodeConverter nodeConverter;

    public NodeService(NodeRepository nodeRepository, NodeConverter nodeConverter) {
        this.nodeRepository = nodeRepository;
        this.nodeConverter = nodeConverter;
    }

    @Transactional
    public List<NodeDto> findByDistance(double latitude, double longitude, double radius) {
        return nodeRepository.findByDistance(latitude, longitude, radius)
            .stream().map(nodeConverter::convert).collect(Collectors.toList());
    }

    @Transactional
    public NodeDto get(long nodeId) {
        return nodeRepository.findById(nodeId).map(nodeConverter::convert).orElse(new NodeDto());
    }

    @Transactional
    public void update(NodeDto node) {
        nodeRepository.save(nodeConverter.convert(node));
    }

    @Transactional
    public void delete(long nodeId) {
        nodeRepository.deleteById(nodeId);
    }

    @Transactional
    public void add(NodeOsm node) {
        nodeRepository.save(nodeConverter.convert(node));
    }
}
