package ru.nsu.fit.malkhanov.dto;

import java.time.Instant;
import java.util.List;

public class NodeDto {
    public long id;
    public Double latitude;
    public Double longitude;
    public String username;
    public Long uid;
    public Boolean visible;
    public Long version;
    public Long changeSet;
    public Instant timestamp;
    public List<TagDto> tags;
}
