package ru.nsu.fit.malkhanov.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.Instant;

@Converter
public class InstantConverter implements AttributeConverter<Instant, Long> {

    @Override
    public Long convertToDatabaseColumn(final Instant instant) {
        return convert(instant);
    }

    @Override
    public Instant convertToEntityAttribute(final Long epoch) {
        return convert(epoch);
    }

    public static Long convert(final Instant instant) {
        return instant == null ? null : instant.getEpochSecond();
    }

    public static Instant convert(final Long epoch) {
        return epoch == null ? null : Instant.ofEpochSecond(epoch);
    }

}
