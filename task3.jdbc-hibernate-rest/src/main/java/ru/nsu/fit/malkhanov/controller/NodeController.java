package ru.nsu.fit.malkhanov.controller;

import org.springframework.web.bind.annotation.*;
import ru.nsu.fit.malkhanov.dto.NodeDto;
import ru.nsu.fit.malkhanov.service.NodeService;

import java.util.List;

@RestController
@RequestMapping("/nodes")
public class NodeController {

    private final NodeService nodeService;

    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @GetMapping("/{id}")
    public NodeDto getNode(@PathVariable long id) {
        return nodeService.get(id);
    }

    @PostMapping
    public void updateNode(@RequestBody NodeDto node) {
        nodeService.update(node);
    }

    @DeleteMapping("/{id}")
    public void deleteNode(@PathVariable long id) {
        nodeService.delete(id);
    }

    @GetMapping("/by-distance")
    public List<NodeDto> findByDistance(@RequestParam double latitude,
                                        @RequestParam double longitude,
                                        @RequestParam double radius) {
        return nodeService.findByDistance(latitude, longitude, radius);
    }
}
