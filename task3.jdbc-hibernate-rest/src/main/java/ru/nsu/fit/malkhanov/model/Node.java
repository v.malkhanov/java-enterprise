package ru.nsu.fit.malkhanov.model;

import lombok.Getter;
import lombok.Setter;
import ru.nsu.fit.malkhanov.converter.InstantConverter;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "node")
public class Node {
    @Id
    @Column(name = "node_id")
    private long id;

    @Column(name = "latitude")
    private Double latitude;

    @Column(name = "longitude")
    private Double longitude;

    @Column(name = "username")
    private String username;

    @Column(name = "uid")
    private Long uid;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "version")
    private Long version;

    @Column(name = "change_set")
    private Long changeSet;

    @Column(name = "timestamp")
    @Convert(converter = InstantConverter.class)
    private Instant timestamp;

    @OneToMany(mappedBy = "node", cascade = CascadeType.ALL)
    private List<Tag> tags;
}
