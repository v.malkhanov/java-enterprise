package ru.nsu.fit.malkhanov.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.nsu.fit.malkhanov.model.jaxb.NodeOsm;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.IOException;

@Slf4j
@Service
public class OsmReader {
    private final static XMLInputFactory factory = XMLInputFactory.newInstance();

    private final NodeService nodeService;
    private XMLStreamReader reader;

    @Value("${osm.path}")
    private String osmArchivePath;

    @Value("${osm.enable}")
    private Boolean enable;

    @PostConstruct
    private void init() {
        if (enable) {
            try {
                var in = new BZip2CompressorInputStream(new FileInputStream(osmArchivePath));
                this.reader = factory.createXMLStreamReader(in);
                this.compute();
                log.info("OSM archive uploaded successfully");
            } catch (IOException | XMLStreamException | JAXBException ex) {
                log.error("Init load OSM", ex);
            }
        }
    }

    public OsmReader(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    private void compute() throws XMLStreamException, JAXBException {
        JAXBContext context = JAXBContext.newInstance(NodeOsm.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        while (hasNextTag("node")) {
            nodeService.add((NodeOsm) unmarshaller.unmarshal(reader));
        }
    }

    private boolean hasNextTag(String tag) throws XMLStreamException {
        while (reader.hasNext()) {
            var element = reader.next();
            if (element == XMLEvent.START_ELEMENT && tag.equals(reader.getLocalName())) {
                return true;
            }
        }
        return false;
    }
}
