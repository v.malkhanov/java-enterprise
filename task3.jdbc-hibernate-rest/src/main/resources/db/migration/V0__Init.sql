DROP TABLE IF EXISTS tag;
DROP TABLE IF EXISTS node;

CREATE TABLE node
(
    node_id     BIGINT              PRIMARY KEY,
    latitude    DOUBLE PRECISION    NOT NULL,
    longitude   DOUBLE PRECISION    NOT NULL,
    username    VARCHAR(256)        NOT NULL,
    uid         BIGINT,
    visible     BOOLEAN,
    version     BIGINT,
    change_set  BIGINT,
    timestamp   BIGINT
);

CREATE TABLE tag
(
    tag_id  BIGINT          PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    key     VARCHAR(256)    NOT NULL,
    value   VARCHAR(256)    NOT NULL,
    node_id BIGINT          REFERENCES node(node_id) ON DELETE CASCADE
);