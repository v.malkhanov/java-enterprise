package dao;

import model.TagDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class TagRepository {
    private final Connection connection;
    private final PreparedStatement preparedStatement;
    private final Statement statement;

    public TagRepository(Connection connection) throws SQLException {
        this.connection = connection;
        this.preparedStatement = connection.prepareStatement("insert into tag (key, value, node_id) values (?, ?, ?)");
        this.statement = connection.createStatement();
    }

    public boolean insert(TagDB tag) throws SQLException {
        var sql = "insert into tag(key, value, node_id) values ('" + tag.getKey() + "', '" +
            tag.getValue().replace("'", "''") + "', " + tag.getNodeId() + ")";
        return statement.execute(sql);
    }

    public boolean preparedInsert(TagDB tag) throws SQLException {
        preparedStatement.setString(1, tag.getKey());
        preparedStatement.setString(2, tag.getValue());
        preparedStatement.setLong(3, tag.getNodeId());
        return preparedStatement.execute();
    }

    public int[] batchInsert(Collection<TagDB> tags) throws SQLException {
        for (var tag : tags) {
            preparedStatement.setString(1, tag.getKey());
            preparedStatement.setString(2, tag.getValue());
            preparedStatement.setLong(3, tag.getNodeId());
            preparedStatement.addBatch();
        }
        return preparedStatement.executeBatch();
    }
}
