package dao;

import model.NodeDB;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collection;

public class NodeRepository {
    private final Connection connection;
    private final PreparedStatement preparedStatement;
    private final Statement statement;

    public NodeRepository(Connection connection) throws SQLException {
        this.connection = connection;
        this.preparedStatement = connection.prepareStatement("insert into node(id, username) values (?, ?)");
        this.statement = connection.createStatement();
    }

    public boolean insert(NodeDB node) throws SQLException {
        var sql = "insert into node(id, username) values (" + node.getId() + ", '" +
            node.getUser().replace("'", "''") + "')";
        return statement.execute(sql);
    }

    public boolean preparedInsert(NodeDB node) throws SQLException {
        preparedStatement.setLong(1, node.getId());
        preparedStatement.setString(2, node.getUser());
        return preparedStatement.execute();
    }

    public int[] batchInsert(Collection<NodeDB> nodes) throws SQLException {
        for (var node : nodes) {
            preparedStatement.setLong(1, node.getId());
            preparedStatement.setString(2, node.getUser());
            preparedStatement.addBatch();
        }
        return preparedStatement.executeBatch();
    }
}
