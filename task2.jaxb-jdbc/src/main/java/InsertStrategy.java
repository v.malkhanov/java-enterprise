public enum InsertStrategy {
    SIMPLE,
    PREPARED,
    BATCH
}
