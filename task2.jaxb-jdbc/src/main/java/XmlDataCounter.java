import model.jaxb.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import javax.xml.stream.events.XMLEvent;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class XmlDataCounter {
    private static final int BATCH_SIZE = 1 << 15;
    private final static XMLInputFactory factory = XMLInputFactory.newInstance();

    private final XMLStreamReader reader;
    private final NodeService nodeService;
    private long count;

    public XmlDataCounter(NodeService nodeService, InputStream in) throws XMLStreamException {
        this.reader = factory.createXMLStreamReader(in);
        this.nodeService = nodeService;
    }

    public void compute(InsertStrategy insertStrategy) throws XMLStreamException, JAXBException, SQLException {
        JAXBContext context = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        this.count = 0L;
        long start = System.nanoTime();
        switch (insertStrategy) {
            case SIMPLE:
                simpleInsert(unmarshaller);
                break;
            case PREPARED:
                preparedInsert(unmarshaller);
                break;
            case BATCH:
                batchInsert(unmarshaller);
        }
        long end = System.nanoTime();
        double seconds = 1.0 * (end - start) / 1e9;

        System.out.printf("Time spent: %.4f%n", seconds);
        System.out.printf("Records per second: %.4f%n", count / seconds);
    }

    private void simpleInsert(Unmarshaller unmarshaller) throws XMLStreamException, JAXBException, SQLException {
        while (hasNextTag("node")) {
            nodeService.insert((Node) unmarshaller.unmarshal(reader));
            ++count;
        }
    }

    private void preparedInsert(Unmarshaller unmarshaller) throws XMLStreamException, JAXBException, SQLException {
        while (hasNextTag("node")) {
            nodeService.preparedInsert((Node) unmarshaller.unmarshal(reader));
            ++count;
        }
    }

    private void batchInsert(Unmarshaller unmarshaller) throws XMLStreamException, JAXBException, SQLException {
        List<Node> nodes = new ArrayList<>(BATCH_SIZE);
        while (hasNextTag("node")) {
            nodes.add((Node) unmarshaller.unmarshal(reader));
            if (nodes.size() == BATCH_SIZE) {
                nodeService.batchInsert(nodes);
                nodes.clear();
            }
            ++count;
        }
        if (!nodes.isEmpty()) {
            nodeService.batchInsert(nodes);
        }
    }

    private boolean hasNextTag(String tag) throws XMLStreamException {
        while (reader.hasNext()) {
            var element = reader.next();
            if (element == XMLEvent.START_ELEMENT && tag.equals(reader.getLocalName())) {
                return true;
            }
        }
        return false;
    }
}
