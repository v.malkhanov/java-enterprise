import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbHelper {
    public static final String INIT_SQL = "V0_Init.sql";
    public static final String URL = "jdbc:postgresql://localhost:5432/osm";
    public static final String USERNAME = "postgres";
    public static final String PASSWORD = "123";

    public static Connection init() throws IOException, SQLException, URISyntaxException {
        var connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
        var resource = DbHelper.class.getClassLoader().getResource(INIT_SQL);
        if (resource != null) {
            var sql = Files.readString(Path.of(resource.toURI()));
            var statement = connection.createStatement();
            statement.execute(sql);
        }
        return connection;
    }
}
