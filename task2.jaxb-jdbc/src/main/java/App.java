import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBException;
import javax.xml.stream.XMLStreamException;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.SQLException;

public class App {
    private final static Logger log = LogManager.getLogger(App.class);

    public static void main(String[] args) {
        if (args.length < 2) {
            System.out.println("1. OSM archive");
            System.out.println("2. Insert strategy (simple, prepared, batch)");
            return;
        }
        var fileName = args[0];
        var insertStrategy = InsertStrategy.valueOf(args[1].toUpperCase());
        try (var connection = DbHelper.init();
             var in = new BZip2CompressorInputStream(new FileInputStream(fileName))) {
            var nodeService = new NodeService(connection);
            var counter = new XmlDataCounter(nodeService, in);
            counter.compute(insertStrategy);
        } catch (XMLStreamException | IOException | JAXBException | SQLException | URISyntaxException ex) {
            log.error("Error", ex);
        }
    }
}
