package model;

import model.jaxb.Node;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NodeDB {
    private long id;
    private String user;

    public NodeDB(long id, String user) {
        this.id = id;
        this.user = user;
    }

    public long getId() {
        return id;
    }

    public String getUser() {
        return user;
    }

    public void setId(long id) {
        this.id = id;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public static NodeDB valueOf(ResultSet rs) throws SQLException {
        return new NodeDB(rs.getLong("id"), rs.getString("username"));
    }

    public static NodeDB valueOf(Node node) {
        return new NodeDB(node.getId().longValue(), node.getUser());
    }
}
