package model;

import model.jaxb.Node;
import model.jaxb.Tag;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TagDB {
    private long id;
    private String key;
    private String value;
    private long nodeId;

    public TagDB(long id, String key, String value, long nodeId) {
        this.id = id;
        this.key = key;
        this.value = value;
        this.nodeId = nodeId;
    }

    public TagDB(String key, String value, long nodeId) {
        this.key = key;
        this.value = value;
        this.nodeId = nodeId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public long getNodeId() {
        return nodeId;
    }

    public void setNodeId(long nodeId) {
        this.nodeId = nodeId;
    }

    public static TagDB valueOf(ResultSet rs) throws SQLException {
        return new TagDB(rs.getLong("id"), rs.getString("key"), rs.getString("value"), rs.getLong("node_id"));
    }

    public static TagDB valueOf(Tag tag, Node node) {
        return new TagDB(tag.getK(), tag.getV(), node.getId().longValue());
    }
}
