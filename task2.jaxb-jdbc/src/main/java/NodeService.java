import dao.NodeRepository;
import dao.TagRepository;
import model.NodeDB;
import model.TagDB;
import model.jaxb.Node;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.stream.Collectors;

public class NodeService {
    private final NodeRepository nodeRepository;
    private final TagRepository tagRepository;

    public NodeService(Connection connection) throws SQLException {
        this.nodeRepository = new NodeRepository(connection);
        this.tagRepository = new TagRepository(connection);
    }

    public void insert(Node node) throws SQLException {
        nodeRepository.insert(NodeDB.valueOf(node));
        for (var tag : node.getTag()) {
            tagRepository.insert(TagDB.valueOf(tag, node));
        }
    }

    public void preparedInsert(Node node) throws SQLException {
        nodeRepository.preparedInsert(NodeDB.valueOf(node));
        for (var tag : node.getTag()) {
            tagRepository.preparedInsert(TagDB.valueOf(tag, node));
        }
    }

    public void batchInsert(Collection<Node> nodes) throws SQLException {
        nodeRepository.batchInsert(nodes.stream().map(NodeDB::valueOf).collect(Collectors.toList()));
        tagRepository.batchInsert(nodes.stream().flatMap(node -> node.getTag().stream()
            .map(tag -> TagDB.valueOf(tag, node))).collect(Collectors.toList()));
    }
}
